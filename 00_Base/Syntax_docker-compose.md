# Ключевые слова docker-compose

Док - _https://docs.docker.com/compose/compose-file/compose-file-v3/_ <br />
Инфо - _https://runebook.dev/ru/docs/docker/compose/compose-file/index_

~~~yaml
version: "3"              # Тег версии - указывает на версию синтаксиса файла docker-compose.yml
services:                 # Основной раздел, где создаются и описываются сервисы (контейнеры docker)
  service_name:           # Произвольное название, которое определяет роль сервиса
    image: alpine:latest  # Указывает на докер образ, который будет использован
    container_name: "nm"  # Имя для созданного контейнера
    hostname: "name"      # Имя хоста внутри контейнера
    restart: always       # Поведения контейнера при падении
    networks:             # Привязка контейнера к сети
      - default           # Имя заданное в настройках networks ниже
    ports:                # Маршрутизируемые порты
      - "8000:80"         # Просто для примера
    volumes:              # Монтирование томов (проброс папок) с хоста в контейнер
      - ./src:/var/test   # Слева директория на основной машине, справа - в контейнере
      - /:/rootfs:ro      # Монтирование папки только на чтение
    environment:          # Задает переменные окружения
      PATH: "src/usr"     # Какой-то рандомный путь
      SQL_PASSWORD: pas   # Какой-то пароль
    user: root            # Задаёт пользователя, от которого будет запускаться и работать контейнер            
    working_dir: /var/app # Определяет положение по умолчанию, откуда будут выполняться команды
    healthcheck:          # Проверка работы
      test: <script>      # Скрипт, который должен вернуть 0, если все ок, или 1 — если все нет.
      interval: <interv>  # как часто запускать проверку.
      timeout: <timeout>  # как долго нужно ждать ответа.
      retries: <retries>  # сколько раз тест должен вернуть отрицательный результат

  # Сборка контейнера из существующего образ (описанного в Dockerfile)
  service_name2:          # Произвольное название, которое определяет роль сервиса
    build: ./app          # Создание образа на основе Dockerfile, расположенного в папке app
    depends_on:           # От какого контейнера зависит сервис:
      - service_name3     # Сервис будет запущен, только после старта service_name3
    command:              # Переопределить команду при запуске контейнера
      - redis-server
      - /redis/redis.conf
    labels:               # Метки - указывают дополнительную информацию
      MAINTAINER: ${MAINTAINER_EMAIL}
      SITE_URL: ${SITE_URL}

  # Или так
  service_name3:          # Произвольное название
    build:                # build — указание на необходимость сборки из Dockerfile.
      context: ./path/    # Путь, где находится Dockerfile относительно docker-compose файла.
      args:
        buildno: 22042001 # buildno — номер для сборки.

# Настройка сети
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 172.28.0.0/16  # Подсеть, в которой будут работать все контейнеры, привязанные к default.
~~~

#### Основные команды
~~~docker-compose
docker compose build                         # Собирает сервисы, описанные в конфигурационных файлах
docker compose up                            # Запускает собранные сервисы
docker compose up -d                         # Запуск контейнеров в фоне с флагом -d
docker compose up --abort-on-container-exit  # При остановке любого сервиса остальные будут остановлены автоматически
docker compose run application make install  # Запустит сервис application и выполнит внутри команду make install
docker compose run application bash          # Запустит сервис и подключится к нему с помощью bash
docker compose run --rm application bash     # Запускаемые контейнеры будут автоматически удаляться
docker compose down                          # Останавливает и удаляет все сервисы запущенные с помощью up
docker compose stop                          # Останавливает, но не удаляет сервисы. Повторный запуск - docker-compose start
~~~
