## Синтаксис Yaml и примеры настройки CI

Документация - _https://docs.gitlab.com/_

#### Структура проекта:

* [Base](00_Base/) - _Базовые конструкции для настройки CI/CD, Dockerfile, docker-compose_
* [Variables](01_Variables/) - _Примеры использования переменных_
* [Include](02_Include/) - _Подключение внешних файлов с помощью директивы include_
* [Extends and !Reference](03_Reference/) - _Повторное использование скриптов и переменных_
* [Runners](04_Runners/) - _Настройка раннеров для Windows, Linux, Docker_
* [Registry](05_ContainerRegistry/) - _Добавление своих образов в Container Registry GitLab_
* 123
