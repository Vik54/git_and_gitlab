# Просто рандомный скрипт для теста докер - образа
listASCII = [(i, chr(i)) for i in range(33, 127)]

# Посимвольный вывод в цикле с разбивкой на 6 колонок.
for clmn1, clmn2, clmn3, clmn4, clmn5, clmn6 in zip(*[iter(sum(listASCII, ()))] *6):
    print('|{:4}  {:3} |{:4}  {:3} |{:4}  {:3}|'.format(clmn1, clmn2, clmn3, clmn4, clmn5, clmn6))
