### Установка раннера на Windows

GitLab Doc - _https://docs.gitlab.com/runner/install/windows.html_

1. Создаеётся папка, где будет храниться раннер и конфиги, например C:\GitLab-Runner:
    ~~~bash
    cd C:\ && mkdir GitLab-Runner && cd GitLab-Runner/
    ~~~

2. Скачивается бинарник по ссылке и помещается в созданную папку C:\GitLab-Runner:   
    _https://docs.gitlab.com/runner/install/windows.html_   <br>
    _https://docs.gitlab.com/runner/install/bleeding-edge.html#download-any-other-tagged-release_
    ~~~shell
    move C:\Users\user\Downloads\gitlab-runner.exe C:\GitLab-Runner
    ~~~

3. Ограничиваются права доступа к каталогу и исполняемому файлу GitLab-Runner:
    - Нажмите на файл (папку) правой кнопкой мыши и выберите Свойства
    - Перейдите на вкладку Безопасность
    - В списке учетных записей, имеющих доступ к файлу, выберите в столбце Запретить нужные флаги

4. Регистрируется раннер на GitLab:
    * Перейти в проект, для которого будет регистрироваться раннер (-ы)
    * Перейти в Settings > CI/CD > Runners + expand
    * Нажать "New project runner" - выбрать Windows
    * Добавить теги для раннера или отключить чекбоксом "Run untagged jobs"
    * Скопировать команду:
    ~~~shell
    .\gitlab-runner.exe register --url https://gitlab.com  --token bla-bla-bla
    ~~~

5. Открыть командную строку от ИМЕНИ АДМИНИСТРАТОРА и зарегистрировать раннер:

    Шаги - _https://docs.gitlab.com/runner/register/index.html_

    Перейти в папку и вставить скопированную команду:
    ~~~shell
    cd .\GitLab-Runner\ && .\gitlab-runner.exe register --url https://gitlab.com  --token bla-bla-bla
    ~~~

    Завершить регистрацию (Шаги для регистрации на сервере GitLab):
    ~~~shell
    -- Enter the GitLab instance URL (for example, https://gitlab.com/):
    >> [https://gitlab.com]: https://gitlab.com/
    -- Verifying runner... is valid                        runner=bla-bla

    -- Enter a name for the runner. This is stored only in the local config.toml file:
    >> [YOUR-LAPTOP]: MY_RUNNER_FOR_WINDOWS

    -- Enter an executor: kubernetes, custom, docker-windows, shell, ssh, virtualbox, 
    docker-autoscaler, docker+machine instance, docker, parallels:
    >> shell

    -- Runner registered successfully. Feel free to start it, but if it's running already
    the config should be automatically reloaded!
    ~~~

6. Установка служб gitlab-runner:
    ~~~shell
    cd C:\GitLab-Runner && .\gitlab-runner.exe install
    ~~~

7. Запуск служб gitlab-runner:
    ~~~shell
    .\gitlab-runner.exe start
    ~~~

8. Валидация gitlab-runner на сайте (Зелёный кружочек):
    ~~~shell
    .\gitlab-runner.exe run
    ~~~

9. Команды gitlab-runner:
    ~~~shell
    .\gitlab-runner.exe --help
    ~~~

10. Конфиги gitlab-runner:
    ~~~shell
    cat .\config.toml
    ~~~

11. Остановка gitlab-runner:
    ~~~shell
    .\gitlab-runner.exe stop
    ~~~

12. Статус службы gitlab-runner:
    ~~~shell
    .\gitlab-runner.exe status
    ~~~
