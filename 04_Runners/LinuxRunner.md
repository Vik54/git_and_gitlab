### Установка раннера на Linux (Ubuntu)

GitLab Doc - _https://docs.gitlab.com/runner/install/linux-manually.html_

1. Установка с помощью бинарника:
    ~~~bash
    # Linux x86-64
    sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
    ~~~

2. Добавляются права на выполнение:   
    ~~~bash
    sudo chmod +x /usr/local/bin/gitlab-runner
    ~~~

3. Создаётся пользователь GitLab (потом можно поменять на рута и т.п.):
    ~~~bash
    sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
    ~~~

4. Устанавливается gitlab-runner:
    ~~~bash
    sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
    ~~~

5. Запускается служба gitlab-runner:
    ~~~bash
    sudo gitlab-runner start
    ~~~

6. Регистрируется раннер на GitLab:
    * Перейти в проект, для которого будет регистрироваться раннер (-ы)
    * Перейти в Settings > CI/CD > Runners + expand
    * Нажать "New project runner" - выбрать Linux (по умолчанию)
    * Добавить теги для раннера или отключить чекбоксом "Run untagged jobs"
    * Скопировать команду:
    ~~~shell
    gitlab-runner register --url https://gitlab.com  --token bla-bla-bla
    ~~~

7. Зарегистрировать раннер в системе:

    Шаги - _https://docs.gitlab.com/runner/register/index.html_

    Вставить скопированную команду (из GitLab):
    ~~~shell
    gitlab-runner register --url https://gitlab.com  --token bla-bla-bla
    ~~~

    Завершить регистрацию (Шаги для регистрации на сервере GitLab):
    ~~~shell
    -- Enter the GitLab instance URL (for example, https://gitlab.com/):
    >> [https://gitlab.com]: https://gitlab.com/
    -- Verifying runner... is valid                        runner=bla-bla

    -- Enter a name for the runner. This is stored only in the local config.toml file:
    >> [YOUR-LAPTOP]: MY_RUNNER_FOR_LINUX

    -- Enter an executor: kubernetes, custom, docker-windows, shell, ssh, virtualbox, 
    docker-autoscaler, docker+machine instance, docker, parallels:
    >> shell

    -- Runner registered successfully. Feel free to start it, but if it's running already
    the config should be automatically reloaded!
    ~~~

8. Убедиться, что всё удалось + Валидация на сайте (Зелёный кружочек):
    ~~~bash
    gitlab-runner verify
    ~~~

9. Запуск служб gitlab-runner:
    ~~~bash
    gitlab-runner start
    ~~~

9. Команды gitlab-runner:
    ~~~shell
    gitlab-runner --help
    ~~~

10. Конфиги gitlab-runner:
    ~~~shell
    cat .\config.toml
    ~~~

11. Остановка gitlab-runner:
    ~~~shell
    gitlab-runner stop
    ~~~

12. Статус службы gitlab-runner:
    ~~~shell
    gitlab-runner status
    ~~~
