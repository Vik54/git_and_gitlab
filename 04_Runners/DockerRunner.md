### Развертывание раннера в Docker

GitLab Doc - _https://docs.gitlab.com/runner/install/docker.html_

1. Загрузка Docker-образа Gitlab Runner с DockerHub:
    ~~~shell
    docker pull gitlab/gitlab-runner
    ~~~

2. Создание тома Docker:
    ~~~shell
    docker volume create gitlab-runner-config
    ~~~

3. Запуск контейнера GitLab Runner, используя только что созданный том:
    ~~~shell
    docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
    ~~~

4. Регистрация раннера:
    * Перейти в проект, для которого будет регистрироваться раннер (-ы)
    * Перейти в Settings > CI/CD > Runners + expand
    * Нажать "..." рядом с кнопкой "New project runner"
    * Скопировать токен

5. Зарегистрировать раннер в системе:

    Шаги - _https://docs.gitlab.com/runner/register/index.html_

    ~~~shell
    docker exec -it gitlab-runner gitlab-runner register

    # URL сервера Gitlab -- https://gitlab.com/ (по умолчанию)
    -- Enter the GitLab instance URL (for example, https://gitlab.com/):
    >> https://gitlab.com/

    # Token: регистрационный токен, скопированный из GitLab --> Runner 
    -- Enter the registration token:
    >> qwerty_qwerty_qwerty

    # Description: произвольное описание Runner
    -- Enter a description for the runner:
    >> [123456789]: DOCKER_RUNNER

    # Tags: теги, которые будут использоваться для запуска задач
    -- Enter tags for the runner (comma-separated):
    >> dock, test, my_runner

    -- Enter optional maintenance note for the runner:
    >> qwerty

    # Executor: тип исполнителя, в данном случае «docker»
    -- Enter an executor: docker-windows, parallels, shell, ssh, virtualbox,
    instance, kubernetes, docker, docker-autoscaler, docker+machine, custom:
    >> docker

    # Docker image: образ Docker, используемый для выполнения задач.
    -- Enter the default Docker image (for example, ruby:2.7):
    >> alpine
    ~~~

7. Остановка и повторный запуск контейнера:
    ~~~bash
    docker start <id_контейнера или имя>    # Флаги по необходимости -i -a и т.п
    docker stop <id_контейнера или имя>
    ~~~
